.Screw Torques According ETA
[cols="^1, ^1, ^1, ^1"]
|===

.2+| Thread diameter 3+| Screw Quality
| 8.8 | 10.9 | 12.9
| M6 | 8 | 10 | 12
| M8 | 20 | 25 | 30
| M10 | 40 | 55 | 65
| M12 | 65 | 90 | 110
| M16 | 160 | 220 | 270
| M20 | 310 | 440 | 520
| M24 | 530 | 750 | 900

|===
